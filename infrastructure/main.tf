provider "aws" {
  version = "~> 5.0"
  
    region = "us-east-1"
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
}
resource "aws_instance" "app_server" {
  ami           = "ami-0005e0cfe09cc9050" 
  instance_type = "t2.micro"

  tags = {
    Name = "flask-api-instance-1"
  }
  user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y
              sudo yum install -y python3
              sudo yum install -y git
              sudo yum install -y python3-pip
              sudo pip3 install flask
              cd /tmp
              git clone https://gitlab.com/mbbhawsar28/terraform_flaskapi.git
              cd /tmp/terraform_flaskapi/app
              python3 app.py
              EOF
}
